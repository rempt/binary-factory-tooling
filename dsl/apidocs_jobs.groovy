// Read the contents of the gathered-jobs.json file a step created for us previously
def jobsToParse = readFileFromWorkspace('apidocs/gathered-jobs.json')
def knownJobs = new groovy.json.JsonSlurper().parseText( jobsToParse )

// Iterate over all of the known jobs and create them!
knownJobs.each {
	// Determine the name for the job
	// Our starting point is that we are producing a bespoke build job
	// For these jobs we also disable cron runs
	def jobName = "${it.project}_Bespoke_apidocs"
	def cronJob = ""

	// However if a version name has been specified then we should use that instead
	if( it.containsKey('version') ) {
		jobName = "${it.project}_${it.version}_apidocs"
		cronJob = "@daily"
	}

	// Read in the necessary Pipeline template
	def pipelineTemplate = readFileFromWorkspace("apidocs/${it.generator}.pipeline")
	
	// Now we can construct our Pipeline script
	// We append a series of variables to the top of it to provide a variety of useful information to the otherwise templated script
	// These appended variables are what makes one build different to the next, aside from the template which was used
	// Due to bespoke jobs not having version/branch specified, we have to have separate paths for this
	def pipelineScript = ""

	// First we will do one for jobs with versions specified...
	if( it.containsKey('version') ) {
		pipelineScript = """
			|def project = "${it.project}"
			|def version = "${it.version}"
			|def branch = "${it.branch}"
			|def repositoriesToClone = "${it.repositoriesToClone}"

			|${pipelineTemplate}""".stripMargin()

	} else {
		// Now we can do the one for bespoke jobs...

		pipelineScript = """
			|def buildParameters = input(
			|	message: 'Which branch shall we build, and what will it be known as?',
			|	ok: 'Begin Build',
			|	parameters: [
			|		string(defaultValue: '', description: '', name: 'Version', trim: true)
			|		string(defaultValue: '', description: '', name: 'Branch', trim: true)
			|	]
			|)

			|def project = "${it.project}"
			|def version = buildParameters.Version ?: ''
			|def branch = buildParameters.Branch ?: ''
			|def repositoriesToClone = "${it.repositoriesToClone}"

			|${pipelineTemplate}""".stripMargin()
	}

	// Actually create the job now
	pipelineJob( jobName ) {
		properties {
			// We don't want to keep build results forever
			// We'll set it to keep the last 10 builds and discard everything else
			buildDiscarder {
				strategy {
					logRotator {
						numToKeepStr("5")
						daysToKeepStr('')
						artifactDaysToKeepStr('')
						artifactNumToKeepStr('')
					}
				}
			}
			// We don't want to be building the same project more than once
			// This is to prevent one project hogging resources
			disableConcurrentBuilds()
		}
		triggers {
			// Use the appropriate cron schedule...
			cron( cronJob )
		}
		// This is where the Pipeline script actually happens :)
		definition {
			cps {
				script( pipelineScript )
				sandbox()
			}
		}
	}
}
